FROM node:6.0
ENV TZ=America/Sao_Paulo
RUN mkdir /app
WORKDIR /app

COPY ["package.json", "/app/"]
RUN npm install
COPY . /app

EXPOSE 3000

CMD npm test
